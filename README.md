# Elasticsearch template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[Elasticsearch](https://www.elastic.co/elasticsearch/) database inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of Elasticsearch (see
[Bastillefile](Bastillefile)) will be installed and the service will be
configured to allow network access.

Please note that you **must** set the `NETWORK_HOST` argument because
elasticsearch will error out otherwise as a jail has no default
loopback interface.

**Due to issues with the used database this has to be a _thick_ jail!**

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

```
# bastille bootstrap https://codeberg.org/wegtam/bastille-elasticsearch
```

## Usage

### 1. Install the default version into a jail

```
# bastille template TARGET wegtam/bastille-elasticsearch --arg NETWORK_HOST=10.8.2.23
```

### 2. Install with custom settings

```
# bastille template TARGET wegtam/bastille-elasticsearch --arg VERSION=7 --arg JAVA_VERSION=8 \
  --arg NETWORK_HOST=192.168.12.3
```

